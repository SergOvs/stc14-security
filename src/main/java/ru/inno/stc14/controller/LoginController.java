package ru.inno.stc14.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    private final PasswordEncoder encoder;
    private final JdbcTemplate jdbc;

    @Autowired
    public LoginController(PasswordEncoder encoder, JdbcTemplate jdbc) {
        this.encoder = encoder;
        this.jdbc = jdbc;
    }

    @PostMapping("/register")
    public String addUser(@RequestParam("login") String login,
                          @RequestParam("pass") String pass) {

        // TODO: 18.02.2019 перенести в сервис
        String passwordHash = encoder.encode(pass);

        // TODO: 18.02.2019 перенести в DAO
        String sql = "insert into \"user\"(login, password, user_role) values (?,?,?)";
        jdbc.update(sql, login, passwordHash, "ROLE_USER");
        return "user";
    }
}
